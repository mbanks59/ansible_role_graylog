import pytest


@pytest.fixture
def graylog_web_scheme(ansible_vars):
    if ansible_vars.get('graylog_http_enable_tls'):
        scheme = "https"
    else:
        scheme = "http"

    return scheme
